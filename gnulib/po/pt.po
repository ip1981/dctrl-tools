# Portuguese translation of the "sh-utils" messages
# Copyright (C) 1996 Free Software Foundation, Inc.
# Ant�nio Jo�o Serras Rendas <arendas@mail.telepac.pt>, 1996
#
msgid ""
msgstr ""
"Project-Id-Version: sh-utils 1.12i\n"
"Report-Msgid-Bugs-To: bug-gnulib@gnu.org\n"
"POT-Creation-Date: 2013-07-01 17:23+0400\n"
"PO-Revision-Date: 1996-11-08 20:03+0100\n"
"Last-Translator: Ant�nio Jo�o Serras Rendas <arendas@mail.telepac.pt>\n"
"Language-Team: Portugu�s <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8-bit\n"

#: gnulib/lib/argp-help.c:147
#, c-format
msgid "ARGP_HELP_FMT: %s value is less than or equal to %s"
msgstr ""

#: gnulib/lib/argp-help.c:220
#, c-format
msgid "%.*s: ARGP_HELP_FMT parameter requires a value"
msgstr ""

#: gnulib/lib/argp-help.c:226
#, c-format
msgid "%.*s: ARGP_HELP_FMT parameter must be positive"
msgstr ""

#: gnulib/lib/argp-help.c:235
#, c-format
msgid "%.*s: Unknown ARGP_HELP_FMT parameter"
msgstr ""

#: gnulib/lib/argp-help.c:247
#, c-format
msgid "Garbage in ARGP_HELP_FMT: %s"
msgstr ""

#: gnulib/lib/argp-help.c:1247
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""

#: gnulib/lib/argp-help.c:1640
msgid "Usage:"
msgstr ""

#: gnulib/lib/argp-help.c:1644
msgid "  or: "
msgstr ""

#: gnulib/lib/argp-help.c:1656
#, fuzzy
msgid " [OPTION...]"
msgstr "Utiliza��o: %s [OP��O]...\n"

#: gnulib/lib/argp-help.c:1683
#, fuzzy, c-format
msgid "Try '%s --help' or '%s --usage' for more information.\n"
msgstr "Tente `%s --help' para mais informa��o.\n"

#: gnulib/lib/argp-help.c:1711
#, c-format
msgid "Report bugs to %s.\n"
msgstr ""

#: gnulib/lib/argp-help.c:1930
#, fuzzy
msgid "Unknown system error"
msgstr "operador bin�rio desconhecido"

#: gnulib/lib/argp-parse.c:81
msgid "give this help list"
msgstr ""

#: gnulib/lib/argp-parse.c:82
msgid "give a short usage message"
msgstr ""

#: gnulib/lib/argp-parse.c:83
msgid "NAME"
msgstr ""

#: gnulib/lib/argp-parse.c:83
msgid "set the program name"
msgstr ""

#: gnulib/lib/argp-parse.c:84
msgid "SECS"
msgstr ""

#: gnulib/lib/argp-parse.c:85
msgid "hang for SECS seconds (default 3600)"
msgstr ""

#: gnulib/lib/argp-parse.c:142
#, fuzzy
msgid "print program version"
msgstr "erro de leitura"

#: gnulib/lib/argp-parse.c:159
msgid "(PROGRAM ERROR) No version known!?"
msgstr ""

#: gnulib/lib/argp-parse.c:612
#, fuzzy, c-format
msgid "%s: Too many arguments\n"
msgstr "demasiados argumentos\n"

#: gnulib/lib/argp-parse.c:755
msgid "(PROGRAM ERROR) Option should have been recognized!?"
msgstr ""

#: gnulib/lib/getopt.c:547 gnulib/lib/getopt.c:576
#, c-format
msgid "%s: option '%s' is ambiguous; possibilities:"
msgstr ""

#: gnulib/lib/getopt.c:624 gnulib/lib/getopt.c:628
#, c-format
msgid "%s: option '--%s' doesn't allow an argument\n"
msgstr ""

#: gnulib/lib/getopt.c:637 gnulib/lib/getopt.c:642
#, c-format
msgid "%s: option '%c%s' doesn't allow an argument\n"
msgstr ""

#: gnulib/lib/getopt.c:685 gnulib/lib/getopt.c:704
#, fuzzy, c-format
msgid "%s: option '--%s' requires an argument\n"
msgstr "demasiados argumentos\n"

#: gnulib/lib/getopt.c:742 gnulib/lib/getopt.c:745
#, fuzzy, c-format
msgid "%s: unrecognized option '--%s'\n"
msgstr "op��o inv�lida `%s'"

#: gnulib/lib/getopt.c:753 gnulib/lib/getopt.c:756
#, fuzzy, c-format
msgid "%s: unrecognized option '%c%s'\n"
msgstr "op��o inv�lida `%s'"

#: gnulib/lib/getopt.c:805 gnulib/lib/getopt.c:808
#, fuzzy, c-format
msgid "%s: invalid option -- '%c'\n"
msgstr "op��o inv�lida `%s'"

#: gnulib/lib/getopt.c:861 gnulib/lib/getopt.c:878 gnulib/lib/getopt.c:1088
#: gnulib/lib/getopt.c:1106
#, fuzzy, c-format
msgid "%s: option requires an argument -- '%c'\n"
msgstr "demasiados argumentos\n"

#: gnulib/lib/getopt.c:934 gnulib/lib/getopt.c:950
#, c-format
msgid "%s: option '-W %s' is ambiguous\n"
msgstr ""

#: gnulib/lib/getopt.c:974 gnulib/lib/getopt.c:992
#, c-format
msgid "%s: option '-W %s' doesn't allow an argument\n"
msgstr ""

#: gnulib/lib/getopt.c:1013 gnulib/lib/getopt.c:1031
#, fuzzy, c-format
msgid "%s: option '-W %s' requires an argument\n"
msgstr "demasiados argumentos\n"

#, fuzzy
#~ msgid "invalid argument %s for %s"
#~ msgstr "argumento inv�lido `%s'"

#, fuzzy
#~ msgid "ambiguous argument %s for %s"
#~ msgstr "falta um argumento a `%s'"

#, fuzzy
#~ msgid "Valid arguments are:"
#~ msgstr "argumento inv�lido `%s'"

#, fuzzy
#~ msgid "program error"
#~ msgstr "erro de leitura"

#~ msgid "write error"
#~ msgstr "erro na escrita"

#, fuzzy
#~ msgid "error while opening \"%s\" for reading"
#~ msgstr "n�o consigo mover `%s' para `%s'"

#, fuzzy
#~ msgid "cannot open backup file \"%s\" for writing"
#~ msgstr "n�o consigo mover `%s' para `%s'"

#, fuzzy
#~ msgid "error reading \"%s\""
#~ msgstr "%s: apagar directoria `%s'? "

#, fuzzy
#~ msgid "error writing \"%s\""
#~ msgstr "%s: apagar directoria `%s'? "

#, fuzzy
#~ msgid "error after reading \"%s\""
#~ msgstr "%s: apagar directoria `%s'? "

#, fuzzy
#~ msgid "fdopen() failed"
#~ msgstr "ficheiros especiais de tipo bloco n�o suportados"

#, fuzzy
#~ msgid "%s subprocess failed"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "regular file"
#~ msgstr "`%s' n�o � um ficheiro normal"

#, fuzzy
#~ msgid "directory"
#~ msgstr "`%s' n�o � uma directoria"

#, fuzzy
#~ msgid "block special file"
#~ msgstr "ficheiros especiais de tipo bloco n�o suportados"

#, fuzzy
#~ msgid "character special file"
#~ msgstr "ficheiros especiais de tipo caracter n�o suportados"

#, fuzzy
#~ msgid "symbolic link"
#~ msgstr "n�o consigo ler liga��o (link) simb�lica `%s'"

#, fuzzy
#~ msgid "weird file"
#~ msgstr "`%s' n�o � um ficheiro normal"

#, fuzzy
#~ msgid "Address family for hostname not supported"
#~ msgstr "ficheiros \"fifo\" n�o suportados"

#, fuzzy
#~ msgid "ai_family not supported"
#~ msgstr "ficheiros \"fifo\" n�o suportados"

#, fuzzy
#~ msgid "ai_socktype not supported"
#~ msgstr "ficheiros \"fifo\" n�o suportados"

#, fuzzy
#~ msgid "System error"
#~ msgstr "erro na escrita"

#, fuzzy
#~ msgid "Unknown error"
#~ msgstr "operador bin�rio desconhecido"

#, fuzzy
#~ msgid "%s: illegal option -- %c\n"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "block size"
#~ msgstr "tipo de ordena��o"

#, fuzzy
#~ msgid "%s exists but is not a directory"
#~ msgstr "`%s' n�o � uma directoria"

#, fuzzy
#~ msgid "cannot change owner and/or group of %s"
#~ msgstr "aviso: n�o consigo mudar para a directoria %s"

#, fuzzy
#~ msgid "cannot create directory %s"
#~ msgstr "n�o consigo obter a directoria actual"

#, fuzzy
#~ msgid "cannot chdir to directory %s"
#~ msgstr "aviso: n�o consigo mudar para a directoria %s"

#, fuzzy
#~ msgid "cannot change permissions of %s"
#~ msgstr "aviso: n�o consigo mudar para a directoria %s"

#, fuzzy
#~ msgid "memory exhausted"
#~ msgstr "mem�ria virtual esgotada"

#, fuzzy
#~ msgid "unable to record current working directory"
#~ msgstr "n�o consigo obter a directoria actual"

#, fuzzy
#~ msgid "Failed to open /dev/zero for read"
#~ msgstr "aviso: n�o consigo mudar para a directoria %s"

#, fuzzy
#~ msgid "cannot create pipe"
#~ msgstr "n�o consigo obter a directoria actual"

#, fuzzy
#~ msgid "Invalid regular expression"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "Invalid character class name"
#~ msgstr "data inv�lida `%s'"

#, fuzzy
#~ msgid "Memory exhausted"
#~ msgstr "mem�ria virtual esgotada"

#, fuzzy
#~ msgid "Invalid preceding regular expression"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "Premature end of regular expression"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "Regular expression too big"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "No previous regular expression"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "character out of range"
#~ msgstr "ficheiros especiais de tipo caracter n�o suportados"

#, fuzzy
#~ msgid "invalid user"
#~ msgstr "\\%c: caracter de escape inv�lido"

#, fuzzy
#~ msgid "invalid group"
#~ msgstr "op��o inv�lida `%s'"

#, fuzzy
#~ msgid "The strings compared were %s and %s."
#~ msgstr "n�o consigo %s `%s' para `%s'"
